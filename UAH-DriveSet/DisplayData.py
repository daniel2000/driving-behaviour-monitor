def seconds_to_minutes(seconds):
    total_mins = seconds / 60
    number_dec = str(total_mins - int(total_mins))[1:]
    total_seconds = float(number_dec) * 0.6*100
    print(str(seconds) + " = " + str(int(total_mins)) + " mins " + "{:.2f}".format(total_seconds) + " seconds")

seconds_to_minutes(680)



