import os

# importing panda library
import pandas as pd


main_dir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\UAH-DRIVESET-v1"


for subdir, dirs, files in os.walk(main_dir):
    #print(subdir)
    #print(dirs)
    #print(files)
    for file in files:
        if file[-4:] == ".txt":
            # readinag given csv file
            # and creating dataframe
            header_names = None
            if file[:-4] == "EVENTS_INERTIAL":
                header_names = ["Timestamp", "Type", "Level", "GPS Latitude", "GPS Longitude", "Date"]
            elif file[:-4] == "EVENTS_LIST_LANE_CHANGES":
                header_names = ["Timestamp", "Type", "Level", "GPS Latitude", "GPS Longitude", "Duration of Lane Change", "Time threshold"]
            elif file[:-4] == "PROC_LANE_DETECTION":
                header_names = ["Timestamp", "X", "Phi", "W", "State"]
            elif file[:-4] == "PROC_OPENSTREETMAP_DATA":
                header_names = ["Timestamp", "Maximum Speed", "Reliability of obtained maxspeed", "Type of road", "Number of lanes", "Estimated current lane", "GPS Latitude", "GPS Longitude", "OSM Delay", "GPS Speed"]
            elif file[:-4] == "PROC_VEHICLE_DETECTION":
                header_names = ["Timestamp", "Distance to ahead vehicle", "Time of impact to ahead vehicle", "Number of Detected vehilces", "GPS Speed"]
            elif file[:-4] == "RAW_ACCELEROMETERS":
                header_names = ["Timestamp", "Boolean of system activated", "Acceleration in X (Gs)", "Acceleration in Y (Gs)","Acceleration in Z (Gs)","Acceleration in X filtered by KF (Gs)","Acceleration in Y filtered by KF (Gs)","Acceleration in Z filtered by KF (Gs)","Roll","Pitch","Yaw"]
            elif file[:-4] == "RAW_GPS":
                header_names = ["Timestamp", "Speed", "Latitude coordinate", "Longitude Coordinate","Altitude","Vertical Accuracy","Horizontal Accuracy","Course","Difcourse"]
            elif file[:-4] == "SEMANTIC_FINAL" or file[:-4] == "SEMANTIC_ONLINE":
                continue


            dataframe1 = pd.read_csv(os.path.join(subdir, file),sep=' ',encoding='utf-8',index_col=False, header=None,names=header_names)
            # storing this dataframe in a csv file
            dataframe1.to_csv(os.path.join(subdir, file)[:-4] + ".csv",index=None,encoding='utf-8')
