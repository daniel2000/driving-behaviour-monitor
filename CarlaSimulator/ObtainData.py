import os
import csv
# importing panda library
import pandas as pd

def table2lags(table, max_lag, min_lag=0, separator='_'):
    """ Given a dataframe, return a dataframe with different lags of all its columns """
    values=[]
    for i in range(min_lag, max_lag + 1):
        values.append(table.shift(i).copy())
        values[-1].columns = [c + separator + str(i) for c in table.columns]
    return pd.concat(values, axis=1)

def obtain_data_lag(lag):
    eventDataframes = []
    main_dir = "C:\\Users\\attar\\Desktop\\Carla Runs\\Data"
    first_file = True
    testcounter = 0
    for subdir, dirs, files in os.walk(main_dir):
        for file in files:
            linearAccDataDir = os.path.join(subdir, file)
            linearAccData = pd.read_csv(linearAccDataDir)

            if first_file:
                eventDataframes.append(pd.DataFrame())
                first_file = False

            counter = 0

            subcounter = 0
            for index, dataRow in linearAccData.iterrows():
                eventEncoded = False
                if counter == 500:
                    # break
                    pass

                if dataRow[4] != -1:
                    #linearAccData.at[counter, "event"] = truthRow[1]
                    eventDataframes[len(eventDataframes) - 1].at[subcounter, "Timestamp"] = dataRow[3]
                    eventDataframes[len(eventDataframes) - 1].at[subcounter, "x"] = dataRow[6]
                    eventDataframes[len(eventDataframes) - 1].at[subcounter, "y"] = dataRow[7]
                    eventDataframes[len(eventDataframes) - 1].at[subcounter, "z"] = dataRow[8]


                    if dataRow[4] == 0:
                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 1
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveRightTurn"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggresiveLeftTurn"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveAcceleration"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveBraking"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveLeftLaneChange"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveRightLaneChange"] = 0
                    elif dataRow[4] == 1:
                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveRightTurn"] = 1
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggresiveLeftTurn"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveAcceleration"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveBraking"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveLeftLaneChange"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveRightLaneChange"] = 0
                    elif dataRow[4] == 2:
                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveRightTurn"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggresiveLeftTurn"] = 1
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveAcceleration"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveBraking"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveLeftLaneChange"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveRightLaneChange"] = 0
                    elif dataRow[4] == 3:
                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveRightTurn"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggresiveLeftTurn"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveAcceleration"] = 1
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveBraking"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveLeftLaneChange"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveRightLaneChange"] = 0
                    elif dataRow[4] == 4:
                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveRightTurn"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggresiveLeftTurn"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveAcceleration"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveBraking"] = 1
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveLeftLaneChange"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveRightLaneChange"] = 0
                    elif dataRow[4] == 5:
                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveRightTurn"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggresiveLeftTurn"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveAcceleration"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveBraking"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveLeftLaneChange"] = 1
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveRightLaneChange"] = 0
                    elif dataRow[4] == 6:
                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveRightTurn"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggresiveLeftTurn"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveAcceleration"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveBraking"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveLeftLaneChange"] = 0
                        eventDataframes[len(eventDataframes) - 1].at[
                            subcounter, "aggressiveRightLaneChange"] = 1
                    else:
                        raise ("Event not found")

                    subcounter += 1
                    eventEncoded = True



                if not eventEncoded:
                    subcounter = 0
                    if not eventDataframes[len(eventDataframes) - 1].empty:
                        eventDataframes.append(pd.DataFrame())



                counter += 1
    # Removing the last empty dataframe
    eventDataframes.pop()

    outputX = pd.DataFrame()
    outputY = pd.DataFrame()

    length_of_dataframe = []

    for dataframe in eventDataframes:
        length_of_dataframe.append([len(dataframe),list(dataframe.iloc[0,4:])])
        # Adding lag
        x = table2lags(dataframe[['x', 'y', 'z']], lag)
        outputX = outputX.append(x[lag:])
        tempDataFrame = dataframe[lag:]
        outputY = outputY.append(tempDataFrame[['nonAggressive', 'aggressiveRightTurn',
                                                'aggresiveLeftTurn', 'aggressiveAcceleration',
                                                'aggressiveBraking', 'aggressiveLeftLaneChange',
                                                'aggressiveRightLaneChange']])

    return outputX, outputY,length_of_dataframe

lag = 9
x,y,lengths = obtain_data_lag(lag)
print(x,y)

pd.to_pickle(x, "X_CARLA_"+str(lag))
pd.to_pickle(y, "y_CARLA_"+str(lag))
pd.to_pickle(lengths,"CARLALENGTHS")