import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn import metrics
from sklearn.model_selection import train_test_split

arrayFile = open("../sampleArray.txt")

array = np.empty((1, 50, 3),dtype=np.float32)
counter = 0

for line in arrayFile:
    values = line.split("\t")
    subcounter = 0
    for value in values:

        if len(value) == 0:
            continue

        if value[-1] == "\n":
            value = value[:len(value) -1 ]

        array[0][counter][subcounter] = value
        subcounter +=1

    counter+=1

def convertToIndividualArray(X_train):
    X, Y = [], []
    temp = np.empty((X_train.shape[0],1, 50, 3),dtype=np.float32)
    for rowNo, row in enumerate(X_train):
        for colNo in range(0, len(row), 3):
            temp2 = int(colNo / 3)
            temp[rowNo,0, temp2, 0] = X_train[rowNo, colNo]
            temp[rowNo,0, temp2, 1] = X_train[rowNo, colNo + 1]
            temp[rowNo,0, temp2, 2] = X_train[rowNo, colNo + 2]

    return temp



X = pd.read_pickle("../InputPickleFiles/49LagAccOnly/X49LagAccData")
y = pd.read_pickle("../InputPickleFiles/49LagAccOnly/y49lagAccData")

X= X.to_numpy(dtype=np.float32)
y= y.to_numpy()
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)

X_test = convertToIndividualArray(X_test)

# Load the TFLite model and allocate tensors.
interpreter = tf.lite.Interpreter(model_path="../ModelFiles/ExactCopy5-Acc0.9961022138595581.tflite")
interpreter.allocate_tensors()

# Get input and output tensors.
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

# Test the model on random input data.

input_shape = input_details[0]['shape']
input_data = X_test

outputDataArr = np.empty((len(y_test),7),dtype=np.float32)
for i in range(len(input_data)):
    test= input_data[i]
    interpreter.set_tensor(input_details[0]['index'], input_data[i])

    interpreter.invoke()

    # The function `get_tensor()` returns a copy of the tensor data.
    # Use `tensor()` in order to get a pointer to the tensor.
    output_data = interpreter.get_tensor(output_details[0]['index'])
    outputDataArr[i] = output_data


y_test = np.argmax(y_test,axis=1)
predictions = np.argmax(outputDataArr,axis=1)
print(metrics.multilabel_confusion_matrix(y_test, predictions))

max = -1

for output in output_data[0]:
    if output >max:
        max = output

finalArray = []
for output in output_data[0]:
    if output == max:
        finalArray.append(1)
    else:
        finalArray.append(0)

print(finalArray)