import csv
import pandas as pd

def resample(input_file,output_file):
    with open(input_file, 'r') as read_obj, \
            open(output_file, 'w', newline='') as write_obj:
        # Create a csv.reader object from the input file object
        csv_reader = csv.reader(read_obj)
        # Create a csv.writer object from the output file object
        csv_writer = csv.writer(write_obj)
        # Read each row of the input csv file as list

        counter = 0
        tempRow = None
        for row in csv_reader:
            if counter == 0:
                # Write the updated row / list to the output file
                csv_writer.writerow(row)
            elif tempRow is not None:
                for i in range(1,5):
                    row[i] = (float(row[i]) + float(tempRow[i]))/2
                csv_writer.writerow(row)
                tempRow = None
            else:
                tempRow = row

            counter+= 1


resample("C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\21\\campoMagnetico_terra.csv","C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\21\\campoMagnetico_terraSampled.csv")

"""
df = pd.read_csv("C:\\Users\\attar\\Downloads\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\16Test\\campoMagnetico_terra.csv")
idx = len(df) - 1 if len(df) % 2 else len(df)
df = df[:idx].groupby(df.index[:idx] // 2).mean()
print(df)
df.to_csv("C:\\Users\\attar\\Downloads\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\16Test\\campoMagnetico_terraSampled.csv")

"""