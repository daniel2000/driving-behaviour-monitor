import io
import itertools
import matplotlib.pyplot as plt
import sklearn

from Helper.HelperClass import *
import tensorflow as tf


from keras.layers import GRU
from keras.layers import Dense, Concatenate, Input
from keras.models import Model

import csv
import pickle

class_names = ["Non Aggressive Event", "Aggressive Right Turn", "Aggressive Left Turn", "Aggressive Acceleration",
               "Aggressive Braking", "Aggressive Left Lane Change", "Aggressive Right Lane Change"]

def plot_to_image(figure):
  """Converts the matplotlib plot specified by 'figure' to a PNG image and
  returns it. The supplied figure is closed and inaccessible after this call."""
  # Save the plot to a PNG in memory.
  buf = io.BytesIO()
  plt.savefig(buf, format='png')
  # Closing the figure prevents it from being displayed directly inside
  # the notebook.
  plt.close(figure)
  buf.seek(0)
  # Convert PNG buffer to TF image
  image = tf.image.decode_png(buf.getvalue(), channels=4)
  # Add the batch dimension
  image = tf.expand_dims(image, 0)
  return image

def plot_confusion_matrix(cm, class_names):
    """
    Returns a matplotlib figure containing the plotted confusion matrix.

    Args:
    cm (array, shape = [n, n]): a confusion matrix of integer classes
    class_names (array, shape = [n]): String names of the integer classes
    """
    figure = plt.figure(figsize=(8, 8))
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    plt.title("Confusion matrix")
    plt.colorbar()
    tick_marks = np.arange(len(class_names))
    plt.xticks(tick_marks, class_names, rotation=45)
    plt.yticks(tick_marks, class_names)



    # Compute the labels from the normalized confusion matrix.
    labels = np.around(cm.astype('float') / cm.sum(axis=1)[:, np.newaxis], decimals=2)


    # Use white text if squares are dark; otherwise black.
    threshold = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        color = "white" if cm[i, j] > threshold else "black"
        temp = str(cm[i, j]) + "," + str(labels[i,j])
        plt.text(j, i, temp, horizontalalignment="center", color=color)

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    return figure

# convert into dataset matrix
def convertToMatrix(data, step):
    X, Y = [], []
    for i in range(len(data) - step):
        d = i + step
        X.append(data[i:d, ])
        Y.append(data[d,])
    return np.array(X), np.array(Y)


def convertToIndividualArray(input_array,numofLag):
    temp = np.empty((input_array.shape[0], numofLag, 3))
    for rowNo, row in enumerate(input_array):
        for colNo in range(0, len(row), 3):
            temp2 = int(colNo / 3)
            temp[rowNo, temp2, 0] = input_array[rowNo, colNo]
            temp[rowNo, temp2, 1] = input_array[rowNo, colNo + 1]
            temp[rowNo, temp2, 2] = input_array[rowNo, colNo + 2]

    return temp


def revert_onehot_encoding(input_array):
    list =  [np.where(r==1)[0][0] for r in input_array]
    return np.array(list)

def convert_to_one_hot_encoding(array):
    output_array = np.zeros((len(array),7))
    for counter,item in enumerate(array):
        output_array[counter][item] = 1

    return output_array

def train_test_model(X_train, y_train, X_test, y_test, numOfLag, epochs):



    # Definition of Model
    input = Input(shape=(numOfLag, 3))
    branch_outputs = []
    for i in range(10):
        # Slicing the ith channel:
        #out = Lambda(lambda x: x[:, i, :])(input)

        # Setting up your per-channel layers (replace with actual sub-models):
        out = GRU(units=7, activation="softmax")(input)
        branch_outputs.append(out)

    # Concatenating together the per-channel results:
    out = Concatenate()(branch_outputs)

    # Adding some further layers (replace or remove with your architecture):
    out = Dense(units=7, activation="softmax")(out)

    # Building model:
    model = Model(inputs=input, outputs=out)
    model.compile(loss='mean_squared_error', optimizer="rmsprop", metrics=['acc'])

    model.fit(X_train, y_train, epochs=epochs, batch_size=32)  # Run with 1 epoch to speed things up for demo purposes

    test_predictions = model.predict(X_test)

    y_test_argmax = np.argmax(y_test, axis=1)
    y_pred_argmax = np.argmax(test_predictions, axis=1)


    confusion_matrix_of_model = sklearn.metrics.confusion_matrix(y_test_argmax, y_pred_argmax)
    multilabel_confusion_matrix_of_model = sklearn.metrics.multilabel_confusion_matrix(y_test_argmax, y_pred_argmax)
    print(confusion_matrix_of_model)
    print(multilabel_confusion_matrix_of_model)


    _, accuracy = model.evaluate(X_test, y_test)

    print("Accuracy: " + str(accuracy))

    return y_test, test_predictions, y_test_argmax, y_pred_argmax

def plot_pr_curve(dirname, y_test,y_pred):
    # precision-recall curve and f1
    # precision recall curve
    precision = dict()
    recall = dict()
    for i in range(7):
        precision[i], recall[i], _ = sklearn.metrics.precision_recall_curve(y_test[:, i],
                                                            y_pred[:, i])
        plt.plot(recall[i], precision[i], lw=2, label=class_names[i])

    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.legend(loc="best")
    plt.title("Precision vs Recall curve")
    #plt.show()
    plt.savefig(dirname + "/PR Curve" + ".png")
    plt.clf()

def plot_roc_curve(dirname, y_test,y_pred):
    # roc curve
    fpr = dict()
    tpr = dict()

    for i in range(7):
        fpr[i], tpr[i], _ = sklearn.metrics.roc_curve(y_test[:, i],
                                      y_pred[:, i])
        plt.plot(fpr[i], tpr[i], lw=2, label=class_names[i])

        plt.xlabel("false positive rate")
        plt.ylabel("true positive rate")
        plt.legend(loc="best")
        plt.title("ROC curve")
        #plt.show()
        plt.savefig(dirname + "/ROC Curve " + str(i) + ".png")
    plt.clf()


def get_distribution_of_events(y_array):
    distribution  = [0,0,0,0,0,0,0]
    for row in range(y_array.shape[0]):
        for i,col in enumerate(y_array[row]):
            if col == 1:
                distribution[i] +=1
                break
    return distribution

def save_to_csv_file(dirname, y_train,y_test,y_pred,y_test_argmax, y_pred_argmax):

    confusion_matrix_of_model = sklearn.metrics.confusion_matrix(y_test_argmax, y_pred_argmax)
    multilabel_confusion_matrix_of_model = sklearn.metrics.multilabel_confusion_matrix(y_test_argmax, y_pred_argmax)

    with open(dirname +'/Output.csv', mode='w') as employee_file:
        employee_writer = csv.writer(employee_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        employee_writer.writerow([dirname])
        employee_writer.writerow(['Accuracy',sklearn.metrics.accuracy_score(y_test_argmax,y_pred_argmax)])
        employee_writer.writerow(['Weighted Precision Score',sklearn.metrics.precision_score(y_test_argmax,y_pred_argmax,average="weighted")])
        employee_writer.writerow(['Weighted Recall Score',sklearn.metrics.recall_score(y_test_argmax,y_pred_argmax,average="weighted")])
        employee_writer.writerow(['Weighted F1 Score',sklearn.metrics.f1_score(y_test_argmax,y_pred_argmax,average="weighted")])
        employee_writer.writerow(['-------------------------------'])
        employee_writer.writerow(['Distribution of Train Events'])
        employee_writer.writerow(class_names)
        employee_writer.writerow(get_distribution_of_events(y_train))
        employee_writer.writerow(['Distribution of Test Events'])
        employee_writer.writerow(class_names)
        employee_writer.writerow(get_distribution_of_events(y_test))

        employee_writer.writerow(['Confusion Matrix'])
        employee_writer.writerow(['','','','','Predicated Class'])
        class_names_row = []
        class_names_row.append('')
        class_names_row.append('')
        class_names_row.extend(class_names)

        employee_writer.writerow(class_names_row)
        for i in range(confusion_matrix_of_model.shape[0]):

            temp_row = []
            if i == 4:
                temp_row.append('Actual Class')
            else:
                temp_row.append('')
            temp_row.append(class_names[i])


            for j in range(confusion_matrix_of_model.shape[1]):
                temp_row.append(confusion_matrix_of_model[i,j])

            employee_writer.writerow(temp_row)
            temp_row.clear()


        employee_writer.writerow([])
        employee_writer.writerow(['----------------------------'])

        employee_writer.writerow(['Multi Label Confusion Matrix'])


        for conf in range (multilabel_confusion_matrix_of_model.shape[0]):
            employee_writer.writerow([class_names[conf]])
            employee_writer.writerow(['','Predicated Class'])
            for i in range(multilabel_confusion_matrix_of_model.shape[1]):
                temp_row = []
                for j in range(multilabel_confusion_matrix_of_model.shape[2]):
                    temp_row.append(multilabel_confusion_matrix_of_model[conf,i,j])



                employee_writer.writerow(temp_row)

                if i == 1:
                    employee_writer.writerow(['Actual Class'])
                temp_row.clear()
            employee_writer.writerow([])

        employee_writer.writerow([])
        employee_writer.writerow(['----------------------------'])

        plot_roc_curve(dirname,y_test,y_pred)
        plot_pr_curve(dirname,y_test,y_pred)


def save_data(filename, X_train, X_test, y_train, y_test):
    with open(filename + '/X_train.pickle', 'wb') as handle:
        pickle.dump(X_train, handle, protocol=pickle.HIGHEST_PROTOCOL)
    with open(filename + '/X_test.pickle', 'wb') as handle:
        pickle.dump(X_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
    with open(filename + '/y_train.pickle', 'wb') as handle:
        pickle.dump(y_train, handle, protocol=pickle.HIGHEST_PROTOCOL)
    with open(filename + '/y_test.pickle', 'wb') as handle:
        pickle.dump(y_test, handle, protocol=pickle.HIGHEST_PROTOCOL)




