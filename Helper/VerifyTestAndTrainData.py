# convert into dataset matrix
import pandas as pd
from sklearn.model_selection import train_test_split
import numpy as np

def convertToMatrix(data, step):
    X, Y = [], []
    for i in range(len(data) - step):
        d = i + step
        X.append(data[i:d, ])
        Y.append(data[d,])
    return np.array(X), np.array(Y)


def convertToIndividualArray(X_train):
    temp = np.empty((X_train.shape[0], 50, 3))
    for rowNo, row in enumerate(X_train):
        for colNo in range(0, len(row), 3):
            temp2 = int(colNo / 3)
            temp[rowNo, temp2, 0] = X_train[rowNo, colNo]
            temp[rowNo, temp2, 1] = X_train[rowNo, colNo + 1]
            temp[rowNo, temp2, 2] = X_train[rowNo, colNo + 2]

    return temp


# Obtaining the csv file having all outputs on 1 coloumn
# X49LagAccData,y49lagAccData = returnXYData(49, 5)

# X49LagAccData.to_pickle("X49LagAccData")
# y49lagAccData.to_pickle("y49lagAccData")

X = pd.read_pickle("../InputPickleFiles/49LagAccOnly/X49LagAccData")
y = pd.read_pickle("../InputPickleFiles/49LagAccOnly/y49lagAccData")

X = X.to_numpy()
y = y.to_numpy()

Xnew = np.append([X[10]],[X[500],X[3000],X[5000]],axis=0)
ynew = np.append([y[10]],[y[500],y[3000],y[5000]],axis=0)



X_train, X_test, y_train, y_test = train_test_split(Xnew, ynew, test_size=0.3)



#X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
#X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))
X_trainConverted = convertToIndividualArray(X_train)
X_testConverted = convertToIndividualArray(X_test)

print("Here")
