import csv
import math

import pandas as pd
from sklearn import preprocessing


def table2lags(table, max_lag, min_lag=0, separator='_'):
    """ Given a dataframe, return a dataframe with different lags of all its columns """
    values=[]
    for i in range(min_lag, max_lag + 1):
        values.append(table.shift(i).copy())
        values[-1].columns = [c + separator + str(i) for c in table.columns]
    return pd.concat(values, axis=1)

def add_column_in_csv(input_file, output_file):
    #Append a column in existing csv using csv.reader / csv.writer classes
    # Open the input_file in read mode and output_file in write mode
    with open(input_file, 'r') as read_obj, \
            open(output_file, 'w', newline='') as write_obj:
        # Create a csv.reader object from the input file object
        csv_reader = csv.reader(read_obj)
        # Create a csv.writer object from the output file object
        csv_writer = csv.writer(write_obj)
        # Read each row of the input csv file as list
        startTime = 11537640270059.0
        counter = 0
        for row in csv_reader:
            if counter == 0:
                # Write the updated row / list to the output file
                csv_writer.writerow(row)
            if counter != 0:
                if float(row[1]) >= startTime + convertSecondsToNanoSeconds(2.0) and float(
                        row[1]) <= startTime + convertSecondsToNanoSeconds(6.5):
                    row.append("non-aggressive event")
                elif float(row[1]) >= startTime + convertSecondsToNanoSeconds(19.5) and float(
                        row[1]) <= startTime + convertSecondsToNanoSeconds(23.5):
                    row.append("aggressive right curve")
                elif float(row[1]) >= startTime + convertSecondsToNanoSeconds(30) and float(
                        row[1]) <= startTime + convertSecondsToNanoSeconds(33.5):
                    row.append("non-aggressive event")
                elif float(row[1]) >= startTime + convertSecondsToNanoSeconds(95) and float(
                    row[1]) <= startTime + convertSecondsToNanoSeconds(98):
                    row.append("aggressive right curve")
                elif float(row[1]) >= startTime + convertSecondsToNanoSeconds(247) and float(
                    row[1]) <= startTime + convertSecondsToNanoSeconds(251.5):
                    row.append("aggressive left turn")
                elif float(row[1]) >= startTime + convertSecondsToNanoSeconds(348.7) and float(
                    row[1]) <= startTime + convertSecondsToNanoSeconds(352.3):
                    row.append("aggressive left turn")
                elif float(row[1]) >= startTime + convertSecondsToNanoSeconds(485) and float(
                    row[1]) <= startTime + convertSecondsToNanoSeconds(489):
                    row.append("non-aggressive event")
                elif float(row[1]) >= startTime + convertSecondsToNanoSeconds(496) and float(
                    row[1]) <= startTime + convertSecondsToNanoSeconds(499.5):
                    row.append("aggressive left turn")
                elif float(row[1]) >= startTime + convertSecondsToNanoSeconds(587) and float(
                        row[1]) <= startTime + convertSecondsToNanoSeconds(590):
                    row.append("aggressive right curve")
                elif float(row[1]) >= startTime + convertSecondsToNanoSeconds(750) and float(
                    row[1]) <= startTime + convertSecondsToNanoSeconds(753.8):
                    row.append("aggressive left turn")
                elif float(row[1]) >= startTime + convertSecondsToNanoSeconds(840.7) and float(
                        row[1]) <= startTime + convertSecondsToNanoSeconds(844):
                    row.append("aggressive right curve")
                elif float(row[1]) >= startTime + convertSecondsToNanoSeconds(980) and float(
                        row[1]) <= startTime + convertSecondsToNanoSeconds(983.2):
                    row.append("aggressive right curve")
                elif float(row[1]) >= startTime + convertSecondsToNanoSeconds(1087.4) and float(
                        row[1]) <= startTime + convertSecondsToNanoSeconds(1090.9):
                    row.append("aggressive left turn")
                elif float(row[1]) >= startTime + convertSecondsToNanoSeconds(1139.8) and float(
                        row[1]) <= startTime + convertSecondsToNanoSeconds(1142):
                    row.append("change aggressive right lane")
                elif float(row[1]) >= startTime + convertSecondsToNanoSeconds(1201) and float(
                        row[1]) <= startTime + convertSecondsToNanoSeconds(1202.9):
                    row.append("change aggressive right lane")
                elif float(row[1]) >= startTime + convertSecondsToNanoSeconds(1211.4) and float(
                        row[1]) <= startTime + convertSecondsToNanoSeconds(1213.5):
                    row.append("change aggressive right lane")

                else:
                    continue

                # Write the updated row / list to the output file
                csv_writer.writerow(row)
            counter +=1

def convertSecondsToNanoSeconds(nanoseconds):
    return nanoseconds * 1000000000


def returnXYData(lag,type,linear_acceleration,columnNo=-1 ):
    # Type 1 for normal
    # Type 2 for only aggressive and non aggressive
    # Type 3 for 7 seperate

    if type == 1:
        # Obtaining data
        data = pd.read_csv('C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\FinalAll - Output - Original.csv')

        # Adding lag
        x = table2lags(data[['gyroscopeX', 'gyroscopeY', 'gyroscopeZ','magneticFieldX','magneticFieldY','magneticFieldZ'
            ,'accelerometerX','accelerometerY','accelerometerZ','linearAccelerationX','linearAccelerationY','linearAccelerationZ']], lag)

        # Label Encoding
        le = preprocessing.LabelEncoder()
        data["event_cat"] = le.fit_transform(data["event"])


        #data["event"] = data["event"].astype('category')
        #data["event_cat"] = data["event"].cat.codes

        # Remove first [lag] rows which include nan values
        data = data[lag:]


        #
        X=x[lag:]
        y=data['event_cat']

        return X, y, le,len(le.classes_)

    elif type == 2:
        data = pd.read_csv('C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\FinalAll - Output - Aggresive-NonAggressive.csv')

        # Adding lag
        x = table2lags(
            data[['gyroscopeX', 'gyroscopeY', 'gyroscopeZ', 'magneticFieldX', 'magneticFieldY', 'magneticFieldZ'
                , 'accelerometerX', 'accelerometerY', 'accelerometerZ', 'linearAccelerationX', 'linearAccelerationY',
                  'linearAccelerationZ']], lag)

        # Label Encoding
        #le = preprocessing.LabelEncoder()
        #data["event_cat"] = le.fit_transform(data["event"])

        # data["event"] = data["event"].astype('category')
        # data["event_cat"] = data["event"].cat.codes

        # Remove first [lag] rows which include nan values
        data = data[lag:]

        #
        X = x[lag:]
        y = data['event']

        return X, y
    elif type == 3:
        data = pd.read_csv('C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\FinalAll - Output - Seperate.csv')

        # Adding lag
        """
        x = table2lags(
            data[['gyroscopeX', 'gyroscopeY', 'gyroscopeZ', 'magneticFieldX', 'magneticFieldY', 'magneticFieldZ'
                , 'accelerometerX', 'accelerometerY', 'accelerometerZ', 'linearAccelerationX', 'linearAccelerationY',
                  'linearAccelerationZ']], lag)
                  """

        # Adding lag
        x = table2lags(data[['accelerometerX', 'accelerometerY', 'accelerometerZ']], lag)
        # Label Encoding
        #le = preprocessing.LabelEncoder()
        #data["event_cat"] = le.fit_transform(data["event"])

        # data["event"] = data["event"].astype('category')
        # data["event_cat"] = data["event"].cat.codes

        # Remove first [lag] rows which include nan values
        data = data[lag:]

        #
        X = x[lag:]

        if columnNo == 0:
            y = data['eventNonAggressive']
        elif columnNo == 1:
            y = data['eventAggressiveAcceleration']
        elif columnNo == 2:
            y = data['eventAggressiveBraking']
        elif columnNo == 3:
            y = data['eventLeftTurn']
        elif columnNo == 4:
            y = data['eventRightTurn']
        elif columnNo == 5:
            y = data['eventAggressiveExchangeRightLane']
        elif columnNo == 6:
            y = data['eventAggressiveExchangeLeftLane']
        elif columnNo == 7:
            y = data[['eventNonAggressive','eventAggressiveAcceleration','eventAggressiveBraking','eventLeftTurn',
                     'eventRightTurn','eventAggressiveExchangeRightLane','eventAggressiveExchangeLeftLane']]
        else:
            raise("Invalid Input")


        return X, y

    elif type == 4:
        data = pd.read_csv(
            'C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\FinalAll - Output - Original - AccelerometerOnly.csv')

        # Adding lag
        x = table2lags(
            data[['accelerometerX', 'accelerometerY', 'accelerometerZ']], lag)

        # Label Encoding
        le = preprocessing.LabelEncoder()
        data["event_cat"] = le.fit_transform(data["event"])

        # data["event"] = data["event"].astype('category')
        # data["event_cat"] = data["event"].cat.codes

        # Remove first [lag] rows which include nan values
        data = data[lag:]

        #
        X = x[lag:]
        y = data['event_cat']

        return X, y
    elif type == 5:

        eventDataframes = []
        #eventDataframes.append(pd.DataFrame())

        driverNo = [16,17,20,21]
        #driverNo = [21]
        startTimes = [11537640270059.0,12893233616460.0,10196945530109.0,11200345835195.0]
        #startTimes = [11200345835195.0]

        for driverNoIndex in range(4):
            print("Obtaing Data for driver: " + str(driverNo[driverNoIndex]))
            accDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-masterLatest\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\acelerometro_terra10Hz.csv"
            linearAccDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-masterLatest\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\aceleracaoLinear_terra10Hz.csv"
            magneticFieldDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-masterLatest\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\campoMagnetico_terra.csv"
            gyroscopeDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-masterLatest\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\giroscopio_terra.csv"
            truthEventDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-masterLatest\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex]) +"\\groundTruth.csv"
            accData = pd.read_csv(accDataDir)
            linearAccData = pd.read_csv(linearAccDataDir)
            magneticFieldData = pd.read_csv(magneticFieldDataDir)
            gyroscopeData = pd.read_csv(gyroscopeDataDir)
            truthEvents = pd.read_csv(truthEventDir)

            eventDataframes.append(pd.DataFrame())

            # Read each row of the input csv file as list
            startTime = startTimes[driverNoIndex]
            counter = 0


            dataframe_to_run = None
            if linear_acceleration:
                dataframe_to_run = linearAccData
            else:
                dataframe_to_run = accData


            subcounter = 0
            for index, dataRow in dataframe_to_run.iterrows():
                if counter == 500:
                    print("ds")
                    pass
                    #break
                if counter != 0:
                    for indexTruthRow, truthRow in truthEvents.iterrows():
                        eventEncoded = False
                        if float(dataRow[1]) >= startTime + convertSecondsToNanoSeconds(float(truthRow[1])) and float(dataRow[1]) <= startTime + convertSecondsToNanoSeconds(float(truthRow[2])):
                            linearAccData.at[counter, "event"] = truthRow[0]
                            eventDataframes[len(eventDataframes)-1].at[subcounter,"timestamp"] = dataRow[0]
                            eventDataframes[len(eventDataframes)-1].at[subcounter,"uptimeNanos"] = dataRow[1]
                            eventDataframes[len(eventDataframes)-1].at[subcounter,"x"] = dataRow[2]
                            eventDataframes[len(eventDataframes)-1].at[subcounter,"y"] = dataRow[3]
                            eventDataframes[len(eventDataframes)-1].at[subcounter,"z"] = dataRow[4]

                            if truthRow[0] == "evento_nao_agressivo":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "curva_direita_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "curva_esquerda_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "aceleracao_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "freada_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "troca_faixa_esquerda_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "troca_faixa_direita_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 1
                            else:
                                raise("Event not found")

                            subcounter+=1
                            eventEncoded = True
                            break

                    if not eventEncoded:
                        subcounter = 0
                        if not eventDataframes[len(eventDataframes)-1].empty:
                            eventDataframes.append(pd.DataFrame())


                counter += 1
            # Removing the last empty dataframe
            eventDataframes.pop()

        outputX = pd.DataFrame()
        outputY = pd.DataFrame()
        length_of_dataframe = []
        for dataframe in eventDataframes:
            length_of_dataframe.append([len(dataframe), list(dataframe.iloc[0, 5:])])
            # Adding lag
            x = table2lags(dataframe[['x', 'y', 'z']], lag)
            outputX = outputX.append(x[lag:])
            tempDataFrame = dataframe[lag:]
            outputY = outputY.append(tempDataFrame[['nonAggressive','aggressiveRightTurn',
                                     'aggresiveLeftTurn','aggressiveAcceleration',
                                     'aggressiveBraking','aggressiveLeftLaneChange',
                           'aggressiveRightLaneChange']])


        return outputX,outputY

    elif type == 6:
        # ALL DATA

        eventDataframes = []
        #eventDataframes.append(pd.DataFrame())

        driverNo = [16,17,20,21]
        startTimes = [11537640270059.0,12893233616460.0,10196945530109.0,11200345835195.0]
        dataType = ["acc", "linearAcc", "magneticField", "gyroscope"]

        for driverNoIndex in range(4):
            accDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\acelerometro_terra.csv"
            linearAccDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\aceleracaoLinear_terra.csv"
            magneticFieldDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\campoMagnetico_terra.csv"
            gyroscopeDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\giroscopio_terra.csv"
            truthEventDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex]) +"\\groundTruth.csv"

            dataDFsList = []
            accData = pd.read_csv(accDataDir)
            linearAccData = pd.read_csv(linearAccDataDir)
            magneticFieldData = pd.read_csv(magneticFieldDataDir)
            gyroscopeData = pd.read_csv(gyroscopeDataDir)

            dataDFsList.append(accData)
            dataDFsList.append(linearAccData)
            dataDFsList.append(magneticFieldData)
            dataDFsList.append(gyroscopeData)

            truthEvents = pd.read_csv(truthEventDir)

            eventDataframes.append(pd.DataFrame())

            # Read each row of the input csv file as list
            startTime = startTimes[driverNoIndex]
            counter = 0


            for index, truthRow in truthEvents.iterrows():
                if index == 2:
                    #break
                    pass
                for z in range(4):
                    subcounter = 0
                    for indexData, dataRow in dataDFsList[z].iterrows():
                        eventEncoded = False
                        if counter != 0:
                            if float(dataRow[1]) >= startTime + convertSecondsToNanoSeconds(float(truthRow[1])) and float(dataRow[1]) <= startTime + convertSecondsToNanoSeconds(float(truthRow[2])):

                                #accData.at[counter, "event"] = truthRow[0]
                                #eventDataframes[len(eventDataframes)-1].at[subcounter,"timestamp"] = dataRow[0]
                                #eventDataframes[len(eventDataframes)-1].at[subcounter,"uptimeNanos"] = dataRow[1]


                                # In magnetic field data
                                if z == 2:
                                    if indexData % 2 == 0:
                                        eventDataframes[len(eventDataframes)-1].at[subcounter,dataType[z] + "X"] = dataRow[2]
                                        eventDataframes[len(eventDataframes)-1].at[subcounter,dataType[z] + "Y"] = dataRow[3]
                                        eventDataframes[len(eventDataframes)-1].at[subcounter,dataType[z] + "Z"] = dataRow[4]
                                else:
                                    eventDataframes[len(eventDataframes) - 1].at[subcounter, dataType[z] + "X"] = dataRow[2]
                                    eventDataframes[len(eventDataframes) - 1].at[subcounter, dataType[z] + "Y"] = dataRow[3]
                                    eventDataframes[len(eventDataframes) - 1].at[subcounter, dataType[z] + "Z"] = dataRow[4]

                                if z == 0:
                                    if truthRow[0] == "evento_nao_agressivo":
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 1
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                                    elif truthRow[0] == "curva_direita_agressiva":
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 1
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                                    elif truthRow[0] == "curva_esquerda_agressiva":
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 1
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                                    elif truthRow[0] == "aceleracao_agressiva":
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 1
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                                    elif truthRow[0] == "freada_agressiva":
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 1
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                                    elif truthRow[0] == "troca_faixa_esquerda_agressiva":
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 1
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                                    elif truthRow[0] == "troca_faixa_direita_agressiva":
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                        eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 1
                                    else:
                                        raise("Event not found")

                                subcounter+=1
                                eventEncoded = True



                            if not eventEncoded:
                                subcounter = 0
                                if not eventDataframes[len(eventDataframes)-1].empty:
                                    eventDataframes.append(pd.DataFrame())
                                    break


                        counter += 1

            # Removing the last empty dataframe
            eventDataframes.pop()

        combinedEventDataFrames = []
        for i in range(len(eventDataframes)):
            if i != 0 and i % 3 == 0:
                eventDataframes[i].reset_index(drop=True,inplace=True)
                eventDataframes[i-1].reset_index(drop=True,inplace=True)
                eventDataframes[i-2].reset_index(drop=True,inplace=True)
                eventDataframes[i-3].reset_index(drop=True,inplace=True)
                combinedEventDataFrames.append(pd.concat(eventDataframes[i-3:i+1],axis=1))

        outputX = pd.DataFrame()
        outputY = pd.DataFrame()
        combined = pd.DataFrame()

        for dataframe in combinedEventDataFrames:
            # Adding lag

            combined = pd.DataFrame()

            # Removing any rows containing nan values
            while(True):
                change = False
                row = dataframe.iloc[-1]
                for cellIndex, value in row.iteritems():
                    if math.isnan(value):
                        dataframe.drop(dataframe.tail(1).index, inplace=True)
                        change = True
                        break

                if not change:
                    break



            for type in dataType:
                x = table2lags(dataframe[[type+'X', type + 'Y', type + 'Z']], lag)
                x=x[lag:]
                x.reset_index(drop=True,inplace=True)
                combined = pd.concat([combined,x],axis=1)

            outputX = outputX.append(combined)
            tempDataFrame = dataframe[lag:]
            outputY = outputY.append(tempDataFrame[['nonAggressive','aggressiveRightTurn',
                                     'aggresiveLeftTurn','aggressiveAcceleration',
                                     'aggressiveBraking','aggressiveLeftLaneChange',
                           'aggressiveRightLaneChange']])


        return outputX,outputY
    elif type == 7:

        eventDataframes = []
        #eventDataframes.append(pd.DataFrame())

        driverNo = [16,17,20,21]
        startTimes = [11537640270059.0,12893233616460.0,10196945530109.0,11200345835195.0]

        for driverNoIndex in range(4):
            accDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\acelerometro_terra.csv"
            linearAccDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\aceleracaoLinear_terra.csv"
            magneticFieldDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\campoMagnetico_terra.csv"
            gyroscopeDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\giroscopio_terra.csv"
            truthEventDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex]) +"\\groundTruth.csv"
            accData = pd.read_csv(accDataDir)
            linearAccData = pd.read_csv(linearAccDataDir)
            magneticFieldData = pd.read_csv(magneticFieldDataDir)
            gyroscopeData = pd.read_csv(gyroscopeDataDir)
            truthEvents = pd.read_csv(truthEventDir)

            eventDataframes.append(pd.DataFrame())

            # Read each row of the input csv file as list
            startTime = startTimes[driverNoIndex]
            counter = 0

            subcounter = 0
            for index, dataRow in accData.iterrows():
                if counter == 500:
                    pass
                    #break
                if counter != 0:
                    for indexTruthRow, truthRow in truthEvents.iterrows():
                        eventEncoded = False
                        if float(dataRow[1]) >= startTime + convertSecondsToNanoSeconds(float(truthRow[1])) and float(dataRow[1]) <= startTime + convertSecondsToNanoSeconds(float(truthRow[2])):
                            accData.at[counter, "event"] = truthRow[0]
                            eventDataframes[len(eventDataframes)-1].at[subcounter,"timestamp"] = dataRow[0]
                            eventDataframes[len(eventDataframes)-1].at[subcounter,"uptimeNanos"] = dataRow[1]
                            eventDataframes[len(eventDataframes)-1].at[subcounter,"x"] = dataRow[2]
                            eventDataframes[len(eventDataframes)-1].at[subcounter,"y"] = dataRow[3]
                            eventDataframes[len(eventDataframes)-1].at[subcounter,"z"] = dataRow[4]

                            if truthRow[0] == "evento_nao_agressivo":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "curva_direita_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "curva_esquerda_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "aceleracao_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "freada_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "troca_faixa_esquerda_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "troca_faixa_direita_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 1
                            else:
                                raise("Event not found")

                            subcounter+=1
                            eventEncoded = True
                            break

                    if not eventEncoded:
                        subcounter = 0
                        if not eventDataframes[len(eventDataframes)-1].empty:
                            eventDataframes.append(pd.DataFrame())


                counter += 1
            # Removing the last empty dataframe
            eventDataframes.pop()

        outputX = pd.DataFrame()
        outputY = pd.DataFrame()

        for dataframe in eventDataframes:
            # Adding lag
            x = table2lags(dataframe[['x', 'y', 'z']], lag)
            outputX = outputX.append(x[lag:])
            tempDataFrame = dataframe[lag:]
            outputY = outputY.append(tempDataFrame[['nonAggressive','aggressiveRightTurn',
                                     'aggresiveLeftTurn','aggressiveAcceleration',
                                     'aggressiveBraking','aggressiveLeftLaneChange',
                           'aggressiveRightLaneChange']])


        return outputX,outputY
    elif type == 8:
        #Dataset data with my data

        eventDataframes = []
        #eventDataframes.append(pd.DataFrame())

        driverNo = [16,17,20,21,50,51,52,53]
        startTimes = [11537640270059.0,12893233616460.0,10196945530109.0,11200345835195.0,47810113647230,48742748670207,49311675833272,50151969867274]

        #driverNo = [50,51,52,53]
        #startTimes = [47810113647230,48742748670207,49311675833272,50151969867274]

        for driverNoIndex in range(8):
            accDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\acelerometro_terra.csv"
            linearAccDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\aceleracaoLinear_terra.csv"
            #magneticFieldDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\campoMagnetico_terra.csv"
            #gyroscopeDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\giroscopio_terra.csv"
            truthEventDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex]) +"\\groundTruth.csv"
            #accData = pd.read_csv(accDataDir)
            linearAccData = pd.read_csv(linearAccDataDir)
            #magneticFieldData = pd.read_csv(magneticFieldDataDir)
            #gyroscopeData = pd.read_csv(gyroscopeDataDir)
            truthEvents = pd.read_csv(truthEventDir)

            eventDataframes.append(pd.DataFrame())

            # Read each row of the input csv file as list
            startTime = startTimes[driverNoIndex]
            counter = 0

            if driverNo[driverNoIndex] == 0:
                print("hello")

            subcounter = 0
            for index, dataRow in linearAccData.iterrows():
                if counter == 500:
                    pass
                    #break
                if counter != 0:
                    for indexTruthRow, truthRow in truthEvents.iterrows():
                        eventEncoded = False
                        if float(dataRow[1]) >= startTime + convertSecondsToNanoSeconds(float(truthRow[1])) and float(dataRow[1]) <= startTime + convertSecondsToNanoSeconds(float(truthRow[2])):
                            linearAccData.at[counter, "event"] = truthRow[0]

                            if driverNo[driverNoIndex] == 50 or driverNo[driverNoIndex] == 51 or driverNo[driverNoIndex] == 52 or driverNo[driverNoIndex] == 53:
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "timestamp"] = dataRow[0]
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "uptimeNanos"] = dataRow[1]
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "x"] = dataRow[6]
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "y"] = dataRow[7]
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "z"] = dataRow[8]
                            else:
                                eventDataframes[len(eventDataframes)-1].at[subcounter,"timestamp"] = dataRow[0]
                                eventDataframes[len(eventDataframes)-1].at[subcounter,"uptimeNanos"] = dataRow[1]
                                eventDataframes[len(eventDataframes)-1].at[subcounter,"x"] = dataRow[2]
                                eventDataframes[len(eventDataframes)-1].at[subcounter,"y"] = dataRow[3]
                                eventDataframes[len(eventDataframes)-1].at[subcounter,"z"] = dataRow[4]

                            if truthRow[0] == "evento_nao_agressivo":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "curva_direita_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "curva_esquerda_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "aceleracao_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "freada_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "troca_faixa_esquerda_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "troca_faixa_direita_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 1
                            else:
                                raise("Event not found")

                            subcounter+=1
                            eventEncoded = True
                            break

                    if not eventEncoded:
                        subcounter = 0
                        if not eventDataframes[len(eventDataframes)-1].empty:
                            eventDataframes.append(pd.DataFrame())


                counter += 1
            # Removing the last empty dataframe
            eventDataframes.pop()

        outputX = pd.DataFrame()
        outputY = pd.DataFrame()

        for dataframe in eventDataframes:
            # Adding lag
            x = table2lags(dataframe[['x', 'y', 'z']], lag)
            outputX = outputX.append(x[lag:])
            tempDataFrame = dataframe[lag:]
            outputY = outputY.append(tempDataFrame[['nonAggressive','aggressiveRightTurn',
                                     'aggresiveLeftTurn','aggressiveAcceleration',
                                     'aggressiveBraking','aggressiveLeftLaneChange',
                           'aggressiveRightLaneChange']])


        return outputX,outputY

    elif type == 9:
        #Dataset data with my data

        eventDataframes = []
        #eventDataframes.append(pd.DataFrame())

        driverNo = [16,17,20,21,0]
        startTimes = [11537640270059.0,12893233616460.0,10196945530109.0,11200345835195.0,0]

        for driverNoIndex in range(5):
            if driverNo[driverNoIndex] == 0:
                accDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\jj3tw8kj6h-2\\sensor_raw.csv"
            else:
                accDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\acelerometro_terra.csv"
            #linearAccDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\aceleracaoLinear_terra.csv"
            #magneticFieldDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\campoMagnetico_terra.csv"
            #gyroscopeDataDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex])+ "\\giroscopio_terra.csv"
            truthEventDir = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\" + str(driverNo[driverNoIndex]) +"\\groundTruth.csv"
            accData = pd.read_csv(accDataDir)
            #linearAccData = pd.read_csv(linearAccDataDir)
            #magneticFieldData = pd.read_csv(magneticFieldDataDir)
            #gyroscopeData = pd.read_csv(gyroscopeDataDir)
            truthEvents = pd.read_csv(truthEventDir)

            eventDataframes.append(pd.DataFrame())

            # Read each row of the input csv file as list
            startTime = startTimes[driverNoIndex]
            counter = 0

            if driverNo[driverNoIndex] == 0:
                print("hello")

            subcounter = 0
            for index, dataRow in accData.iterrows():
                if counter == 500:
                    pass
                    #break
                if counter != 0:
                    for indexTruthRow, truthRow in truthEvents.iterrows():
                        eventEncoded = False
                        if driverNo[driverNoIndex] == 0:
                            eventDataframes[len(eventDataframes) - 1].at[subcounter, "x"] = dataRow[4]
                            eventDataframes[len(eventDataframes) - 1].at[subcounter, "y"] = dataRow[5]
                            eventDataframes[len(eventDataframes) - 1].at[subcounter, "z"] = dataRow[6]

                            if dataRow[0] == "curva_direita_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif dataRow[0] == "curva_esquerda_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif dataRow[0] == "aceleracao_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif dataRow[0] == "freada_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            else:
                                raise("Event not found")

                            subcounter+=1
                            eventEncoded = True
                            break

                        elif float(dataRow[1]) >= startTime + convertSecondsToNanoSeconds(float(truthRow[1])) and float(dataRow[1]) <= startTime + convertSecondsToNanoSeconds(float(truthRow[2])):
                            accData.at[counter, "event"] = truthRow[0]

                            if driverNo[driverNoIndex] == 0 or driverNo[driverNoIndex] == 1 or driverNo[driverNoIndex] == 2:
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "timestamp"] = dataRow[0]
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "uptimeNanos"] = dataRow[1]
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "x"] = dataRow[6]
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "y"] = dataRow[7]
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "z"] = dataRow[8]
                            else:
                                eventDataframes[len(eventDataframes)-1].at[subcounter,"timestamp"] = dataRow[0]
                                eventDataframes[len(eventDataframes)-1].at[subcounter,"uptimeNanos"] = dataRow[1]
                                eventDataframes[len(eventDataframes)-1].at[subcounter,"x"] = dataRow[2]
                                eventDataframes[len(eventDataframes)-1].at[subcounter,"y"] = dataRow[3]
                                eventDataframes[len(eventDataframes)-1].at[subcounter,"z"] = dataRow[4]

                            if truthRow[0] == "evento_nao_agressivo":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "curva_direita_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "curva_esquerda_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "aceleracao_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "freada_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "troca_faixa_esquerda_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 1
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 0
                            elif truthRow[0] == "troca_faixa_direita_agressiva":
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "nonAggressive"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggresiveLeftTurn"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveAcceleration"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveBraking"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveLeftLaneChange"] = 0
                                eventDataframes[len(eventDataframes) - 1].at[subcounter, "aggressiveRightLaneChange"] = 1
                            else:
                                raise("Event not found")

                            subcounter+=1
                            eventEncoded = True
                            break

                    if not eventEncoded:
                        subcounter = 0
                        if not eventDataframes[len(eventDataframes)-1].empty:
                            eventDataframes.append(pd.DataFrame())


                counter += 1
            # Removing the last empty dataframe
            eventDataframes.pop()

        outputX = pd.DataFrame()
        outputY = pd.DataFrame()

        for dataframe in eventDataframes:
            # Adding lag
            x = table2lags(dataframe[['x', 'y', 'z']], lag)
            outputX = outputX.append(x[lag:])
            tempDataFrame = dataframe[lag:]
            outputY = outputY.append(tempDataFrame[['nonAggressive','aggressiveRightTurn',
                                     'aggresiveLeftTurn','aggressiveAcceleration',
                                     'aggressiveBraking','aggressiveLeftLaneChange',
                           'aggressiveRightLaneChange']])


        return outputX,outputY



input = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\16 - Copy\\acelerometro_terra.csv"
output = "C:\\Users\\attar\\Desktop\\FYP\\Datasets\\driverBehaviorDataset-master\\driverBehaviorDataset-master\\data\\16 - Copy\\acelerometro_terraTimed.csv"
#add_column_in_csv(input,output)