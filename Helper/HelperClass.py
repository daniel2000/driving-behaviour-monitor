import numpy as np
from sklearn.metrics import confusion_matrix


def confusionMatrix(yTest, yPred):
    print("Confusion Matrix:  x-axis = Predicated Label, y-axis = True Label")
    confMatrix = confusion_matrix(yTest, yPred)
    print(confMatrix)
    return confMatrix

    #print("Precision" ,averageType , "Score = ", precision_score(yTest, yPred, average=averageType))


def calculateMicroPrecision(confusionMatrix):
    Tp = 0
    Fp = 0
    for i in range(len(confusionMatrix)):
        Tp += confusionMatrix[i][i]
        for j in range(len(confusionMatrix)):
            if j != i:
                Fp += confusionMatrix[i][j]

    return Tp / (Tp+Fp)

def multiclassConfusionMatrix(y_test,y_pred):

    confusionMatrix = np.empty([7, 7])

    for i in range(len(y_pred)):
        truthColNo = -1
        predColNo = -1
        found = False
        for j in range(len(y_pred[0])):
            if y_pred[i, j] == 1 and y_test.iat[i, j] == 1:
                confusionMatrix[j, j] += 1
                found = True
                break
            if y_pred[i, j] == 1:
                predColNo = j
            if y_test.iat[i, j] == 1:
                truthColNo = j

        if not found:
            confusionMatrix[truthColNo, predColNo] += 1

    np.set_printoptions(suppress=True)
    print(confusionMatrix)

def binarizePredicitions(y_pred):
    # Choosing the highest probablity for a given data point, setting that coloumn to 1 and the rest to 0
    for i in range(len(y_pred)):
        maximum = max(y_pred[i])
        for j in range(len(y_pred[i])):
            if y_pred[i][j] == maximum:
                y_pred[i][j] = 1
            else:
                y_pred[i][j] = 0
    return y_pred

