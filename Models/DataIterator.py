import os
import pandas as pd
from sklearn import preprocessing


def print_data_disrtibution(input_array):
    histogram = [0, 0, 0, 0, 0, 0, 0]

    for row in input_array.iterrows():
        for c, col in enumerate(row[1]):
            if col == 1:
                histogram[c] += 1
                break

    print(histogram)



directory = 'Data'
for filename in os.listdir(directory):
    if filename[0] == "y" and filename[3:] != "AccData":
        file = pd.read_pickle("Data/" + filename)
        print(filename + " : " + str(len(file)))

        print_data_disrtibution(file)



files = ["CARLALENGTHS","LengthsLINEARDATA","LengthsUAH"]
for file in files:

    lengths = pd.read_pickle(file)

    if file =="CARLALENGTHS":

        for length in lengths:
            for c, col in enumerate(length[1]):
                if col == 1:
                    length[1] = c
                    break

    array =  [0,0,0,0,0,0,0]
    total_no = [0,0,0,0,0,0,0]
    for length in lengths:
        array[length[1]] += length[0]
        total_no[length[1]] +=1


    for i in range(len(array)):
        array[i] = (array[i]/total_no[i]) / 10

    print(file +" Time: " + str(array))
    print(file +" Histogram: " + str(total_no))