
import os
import pandas as pd

directory = 'Data'

#data_to_combine = [['X9LinearAccData','X_UAH_9',9],['y9LinearAccData','y_UAH_9',9],['X12LinearAccData','X_UAH_12',12],['y12LinearAccData','y_UAH_12',12]]
data_to_combine = [['X_CARLA_12','X_UAH_12',12],['y_CARLA_12','y_UAH_12',12],['X_CARLA_9','X_UAH_9',9],['y_CARLA_9','y_UAH_9',9]]

for combination in data_to_combine:
    file_0 = pd.read_pickle("Data/" + combination[0])
    file_1 = pd.read_pickle("Data/" + combination[1])

    combined_dataframe = pd.concat([file_0,file_1])

    pd.to_pickle(combined_dataframe,"Data/" + combination[0][0] + str(combination[2]) + "lagUAHCARLAData")


