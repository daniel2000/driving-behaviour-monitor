# https://www.datatechnotes.com/2018/12/rnn-example-with-keras-simplernn-in.html
import datetime
import os
from os import path

from keras.callbacks import LambdaCallback
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split, StratifiedKFold
from tensorboard.plugins.hparams import api as hp
from tensorflow.python.keras.utils.vis_utils import plot_model

from Helper.HelperFile import *
from Helper.FormatAndObtainData import *
from Helper.ModelConverterToTFLITE import convertModelToTFLITE
from TensorBoardHelperFiles import *

"""
Proposed RNN topology. 
The inputs take into account THREE VECTORS of 50 values each. 
There is ONE HIDDEN layer with RECURRENCE (GRU) (with 2, 5 and 10 neurons). 

The output is mapped as a multi-label problem; 
each output neuron is linked to an event type, as shown in Table I.
"""
testModel = None

noOfNeuronsInOutputLayer = 7



HP_NUM_UNITS = hp.HParam('num_units', hp.Discrete([7]))
HP_BATCH_SIZE = hp.HParam('batch_size', hp.Discrete([32]))
HP_ACT_FUNC = hp.HParam('activation_function', hp.Discrete(["softmax"]))
HP_OPTIMIZER = hp.HParam('optimizer', hp.Discrete(["rmsprop"]))
HP_EPOCHS = hp.HParam('epochs', hp.Discrete([300]))
HP_LAG = hp.HParam('lag', hp.Discrete([50]))




def train_test_model(hparams):
    class_names = ["Non Aggressive Event", "Aggresive Right Turn", "Aggresive Left Turn", "Aggresive Acceleration",
                   "Aggresive Braking", "Aggresive Left Lane Change", "Aggresive Right Lane Change"]
    def log_confusion_matrix(epoch, logs):
        # Use the model to predict the values from the validation dataset.
        test_pred_raw = model.predict(X_test)
        test_pred = np.argmax(test_pred_raw, axis=1)
        y_test2 = np.argmax(y_test,axis=1)
        # Calculate the confusion matrix.
        cm = sklearn.metrics.confusion_matrix(y_test2, test_pred)
        # Log the confusion matrix as an image summary.
        figure = plot_confusion_matrix(cm, class_names=class_names)
        cm_image = plot_to_image(figure)

        # Log the confusion matrix as an image summary.
        with file_writer_cm.as_default():
            tf.summary.image("Confusion Matrix", cm_image, step=epoch)

    # Define the per-epoch callback.
    cm_callback = LambdaCallback(on_epoch_end=log_confusion_matrix)

    input = Input(shape=(lag, 3))
    input_branch_outputs = []
    hidden_branch_outputs = []
    for i in range(3):
        # Slicing the ith channel:
        #out = Lambda(lambda x: x[:, i, :])(input)

        # Setting up your per-channel layers (replace with actual sub-models):
        out = GRU(units=10, activation=hparams[HP_ACT_FUNC],return_sequences=True)(input)
        input_branch_outputs.append(out)

    # Concatenating together the per-channel results:
    inputLayerOutput = Concatenate()(input_branch_outputs)

    for i in range(10):
        # Slicing the ith channel:
        #out = Lambda(lambda x: x[:, i, :])(input)

        # Setting up your per-channel layers (replace with actual sub-models):
        out = GRU(units=7, activation=hparams[HP_ACT_FUNC])(inputLayerOutput)
        hidden_branch_outputs.append(out)

    # Concatenating together the per-channel results:
    hiddenLayerOutput = Concatenate()(hidden_branch_outputs)

    # Adding some further layers (replace or remove with your architecture):
    out = Dense(units=7, activation=hparams[HP_ACT_FUNC])(hiddenLayerOutput)

    # Building model:
    model = Model(inputs=input, outputs=out)
    model.compile(loss='mean_squared_error', optimizer=hparams[HP_OPTIMIZER], metrics=['acc'])
    plot_model(model, to_file='Model_Image/model_plot' + dateAndTimeNow + '.png', show_shapes=True, show_layer_names=True)

    model.fit(X_train, y_train, epochs=hparams[HP_EPOCHS], batch_size=hparams[HP_BATCH_SIZE], callbacks=[
        tf.keras.callbacks.TensorBoard(

            "logs/fit/" + log_dir_name  + dateAndTimeNow),cm_callback])  # Run with 1 epoch to speed things up for demo purposes
    y_pred = model.predict(X_test)


    y_test_argmax = np.argmax(y_test, axis=1)
    y_pred_argmax = np.argmax(y_pred, axis=1)

    confusion_matrix_of_model = sklearn.metrics.confusion_matrix(y_test_argmax, y_pred_argmax)
    multilabel_confusion_matrix_of_model = sklearn.metrics.multilabel_confusion_matrix(y_test_argmax, y_pred_argmax)
    print(confusion_matrix_of_model)
    print(multilabel_confusion_matrix_of_model)



    _, accuracy = model.evaluate(X_test, y_test)
    convertModelToTFLITE(model,model_save_name + str(accuracy))

    print("Accuracy: " + str(accuracy))

    return accuracy,y_test, y_pred, y_test_argmax, y_pred_argmax


def run(run_dir, hparams):
    with tf.summary.create_file_writer(run_dir).as_default():
        hp.hparams(hparams)  # record the values used in this trial
        accuracy, y_test, y_pred, y_test_argmax, y_pred_argmax = train_test_model(hparams)
        tf.summary.scalar('accuracy', accuracy, step=1)
    return y_test, y_pred, y_test_argmax, y_pred_argmax

session_num = 0

for num_units in HP_NUM_UNITS.domain.values:
    for batch_size in HP_BATCH_SIZE.domain.values:
        for act_func in HP_ACT_FUNC.domain.values:
            for optimizer in HP_OPTIMIZER.domain.values:
                for epochs in HP_EPOCHS.domain.values:
                    for lag in HP_LAG.domain.values:
                        hparams = {
                            HP_NUM_UNITS: num_units,
                            HP_BATCH_SIZE: batch_size,
                            HP_ACT_FUNC: act_func,
                            HP_OPTIMIZER: optimizer,
                            HP_EPOCHS: epochs,
                            HP_LAG: lag,
                        }
                        dataset_name = "AccData"
                        model_save_name = str(lag) + dataset_name + str(epochs)
                        log_dir_name =dataset_name +str(lag) + "/"
                        csv_dir_name = "LAG" +dataset_name +"Data"
                        main_dir_name = "CSVFiles/KFoldStratified"+ str(lag) + csv_dir_name  + str(
                            datetime.datetime.now().strftime("%m.%d.%Y, %H.%M.%S"))
                        os.mkdir(main_dir_name)
                        """
                        X = pd.read_pickle("Data/X_CARLA_" + str(lag - 1))
                        y = pd.read_pickle("Data/y_CARLA_" + str(lag - 1))
                       """
                        if path.exists("Data/X" + str(lag - 1) + "AccData") == True:
                            X = pd.read_pickle("Data/X" + str(lag - 1) + "AccData")
                            y = pd.read_pickle("Data/y" + str(lag - 1) + "AccData")
                        else:
                            # Obtaining the csv file having all outputs on 1 coloumn
                            X, y = returnXYData(lag - 1, 5,False)

                            X.to_pickle("Data/X" + str(lag - 1) + "AccData")
                            y.to_pickle("Data/y" + str(lag - 1) + "AccData")
                        

                        X = X.to_numpy()
                        y = y.to_numpy()

                        counter = 0

                        skf = StratifiedKFold(n_splits=5, shuffle=True)

                        y = revert_onehot_encoding(y)

                        for train_index, test_index in skf.split(X, y):
                            total_dir = main_dir_name + "/Run_" + str(counter)
                            os.mkdir(total_dir)
                            counter += 1
                            epochs = epochs
                            numofLag = lag

                            X_train, X_test = X[train_index], X[test_index]
                            y_train, y_test = y[train_index], y[test_index]

                            y_train = convert_to_one_hot_encoding(y_train)
                            y_test = convert_to_one_hot_encoding(y_test)

                            X_train = convertToIndividualArray(X_train, numofLag)
                            X_test = convertToIndividualArray(X_test, numofLag)

                            save_data(total_dir, X_train, X_test, y_train, y_test)

                            dateAndTimeNow = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

                            logdir = "logs/image/"+log_dir_name + dateAndTimeNow
                            # Define the basic TensorBoard callback.
                            tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=logdir)
                            file_writer_cm = tf.summary.create_file_writer(logdir + '/cm')

                            with tf.summary.create_file_writer('logs/hparam_tuning/' +log_dir_name + dateAndTimeNow).as_default():
                                hp.hparams_config(
                                    hparams=[HP_NUM_UNITS, HP_BATCH_SIZE, HP_ACT_FUNC, HP_EPOCHS, HP_LAG],
                                    metrics=[hp.Metric('accuracy', display_name='Accuracy')],
                                )

                            run_name = "run-%d" % session_num
                            print('--- Starting trial: %s' % run_name)
                            print({h.name: hparams[h] for h in hparams})
                            y_test, y_pred, y_test_argmax, y_pred_argmax = run('/logs/hparam_tuning/' +log_dir_name + dateAndTimeNow + run_name, hparams)
                            session_num += 1

                            save_to_csv_file(total_dir, y_train, y_test, y_pred, y_test_argmax, y_pred_argmax)





