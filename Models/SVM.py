from sklearn import metrics
from sklearn.metrics import confusion_matrix, precision_score
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from Helper.FormatAndObtainData import returnXYData
from sklearn.svm import SVC
import pandas as pd
from sklearn import svm
from sklearn.model_selection import train_test_split, StratifiedKFold
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score,f1_score
from os import path

import numpy as np

def convertToIndividualArray(X_train):
    temp = np.empty((X_train.shape[0], 50, 3))
    for rowNo, row in enumerate(X_train):
        for colNo in range(0, len(row), 3):
            temp2 = int(colNo / 3)
            temp[rowNo, temp2, 0] = X_train[rowNo, colNo]
            temp[rowNo, temp2, 1] = X_train[rowNo, colNo + 1]
            temp[rowNo, temp2, 2] = X_train[rowNo, colNo + 2]

    return temp

def revert_onehot_encoding(input_array):
    list =  [np.where(r==1)[0][0] for r in input_array]
    return np.array(list)


def convert_to_one_hot_encoding(array):
    output_array = np.zeros((len(array),7))
    for counter,item in enumerate(array):
        output_array[counter][item] = 1

    return output_array

lag =10


X = pd.read_pickle("Data/X" + str(lag-1) + "lagUAHCARLAData")
y = pd.read_pickle("Data/y"+ str(lag-1) + "lagUAHCARLAData")
"""
X = pd.read_pickle("Data/X_CARLA_" + str(lag-1))
y = pd.read_pickle("Data/y_CARLA_" + str(lag-1))








if path.exists("Data/X" + str(lag - 1) + "LinearAccData") == True:
    X = pd.read_pickle("Data/X" + str(lag - 1) + "LinearAccData")
    y = pd.read_pickle("Data/y" + str(lag - 1) + "LinearAccData")
else:
    # Obtaining the csv file having all outputs on 1 coloumn
    X, y,lengths = returnXYData(lag - 1, 5,True)

    X.to_pickle("Data/X" + str(lag - 1) + "LinearAccData")
    y.to_pickle("Data/y" + str(lag - 1) + "LinearAccData")
"""

X = X.to_numpy()
y = y.to_numpy()
y = revert_onehot_encoding(y)
svm = svm.SVC(C=20,kernel='rbf')

skf_5fold_shuffled = StratifiedKFold(n_splits=5,shuffle=True)

for train_index, test_index in skf_5fold_shuffled.split(X, y):
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]

    svm.fit(X_train,y_train)

    y_pred = svm.predict(X_test)

    print(confusion_matrix(y_test,y_pred))
    print(f1_score(y_test,y_pred,average="weighted"))


