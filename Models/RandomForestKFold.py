from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import multilabel_confusion_matrix, precision_score, accuracy_score, f1_score
from sklearn.model_selection import train_test_split, StratifiedKFold
from Helper.FormatAndObtainData import *
from sklearn.model_selection import GridSearchCV
from os import path


import numpy as np

def convertToIndividualArray(X_train):
    temp = np.empty((X_train.shape[0], 50, 3))
    for rowNo, row in enumerate(X_train):
        for colNo in range(0, len(row), 3):
            temp2 = int(colNo / 3)
            temp[rowNo, temp2, 0] = X_train[rowNo, colNo]
            temp[rowNo, temp2, 1] = X_train[rowNo, colNo + 1]
            temp[rowNo, temp2, 2] = X_train[rowNo, colNo + 2]

    return temp

def revert_onehot_encoding(input_array):
    list =  [np.where(r==1)[0][0] for r in input_array]
    return np.array(list)


def convert_to_one_hot_encoding(array):
    output_array = np.zeros((len(array),7))
    for counter,item in enumerate(array):
        output_array[counter][item] = 1

    return output_array


# https://datascience.stackexchange.com/questions/24108/multiple-time-series-predictions-with-random-forests-in-python
# https://towardsdatascience.com/random-forest-in-python-24d0893d51c0

#X50LagAccOnly,y50lagAccOnly = returnXYData(50,5)+
#lag = 50,25,13
lag = 3
"""
X = pd.read_pickle("Data/X_UAH_5")
y = pd.read_pickle("Data/Y_UAH_9")
"""
if path.exists("Data/X" + str(lag - 1) + "Acc10HzData") == True:
    X = pd.read_pickle("Data/X" + str(lag - 1) + "Acc10HzData")
    y = pd.read_pickle("Data/y" + str(lag - 1) + "Acc10HzData")
else:
    # Obtaining the csv file having all outputs on 1 coloumn
    X, y,lengths = returnXYData(lag - 1, 5,True)

    X.to_pickle("Data/X" + str(lag - 1) + "Acc10HzData")
    y.to_pickle("Data/y" + str(lag - 1) + "Acc10HzData")

X = X.to_numpy()
y = y.to_numpy()


y = revert_onehot_encoding(y)

skf_5fold_shuffled = StratifiedKFold(n_splits=5,shuffle=True)

#X_train, X_test, y_train, y_test = train_test_split( X, y, test_size=0.2)
parameters = {'n_estimators':[500],'max_features':["sqrt"]}

random_forest=RandomForestClassifier()

clf = GridSearchCV(random_forest,parameters,n_jobs=-1,cv=skf_5fold_shuffled,scoring=['accuracy','precision_weighted','recall_weighted','f1_weighted'],refit=False,verbose=3,return_train_score=True)
clf.fit(X,y)

#y_pred = clf.predict(X_test)

#print("Acc:" + str(accuracy_score(y_test,y_pred)))
#print("F1 Score Weighted:" + str(f1_score(y_test,y_pred,average='weighted')))

print("LAG = " + str(lag))
print(clf.cv_results_.keys())
for key in clf.cv_results_.keys():
    print(str(key) + ":" + str(clf.cv_results_[key]))


"""
print(clf.cv_results_['param_n_estimators'])
print(clf.cv_results_['params'])
print(clf.cv_results_['split0_test_score'])
print(clf.cv_results_['split1_test_score'])
print(clf.cv_results_['split2_test_score'])
print(clf.cv_results_['split3_test_score'])
print(clf.cv_results_['split4_test_score'])
print("Mean Test Score")
print(clf.cv_results_['mean_test_score'])
print(clf.cv_results_['std_test_score'])
print(clf.cv_results_['rank_test_score'])
"""