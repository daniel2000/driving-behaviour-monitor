from sklearn import metrics
from sklearn.metrics import confusion_matrix, precision_score
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from Helper.FormatAndObtainData import returnXYData
from sklearn.svm import SVC
import pandas as pd
from sklearn import svm
from sklearn.model_selection import train_test_split, StratifiedKFold
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score,f1_score
from os import path

import numpy as np

def convertToIndividualArray(X_train):
    temp = np.empty((X_train.shape[0], 50, 3))
    for rowNo, row in enumerate(X_train):
        for colNo in range(0, len(row), 3):
            temp2 = int(colNo / 3)
            temp[rowNo, temp2, 0] = X_train[rowNo, colNo]
            temp[rowNo, temp2, 1] = X_train[rowNo, colNo + 1]
            temp[rowNo, temp2, 2] = X_train[rowNo, colNo + 2]

    return temp

def revert_onehot_encoding(input_array):
    list =  [np.where(r==1)[0][0] for r in input_array]
    return np.array(list)


def convert_to_one_hot_encoding(array):
    output_array = np.zeros((len(array),7))
    for counter,item in enumerate(array):
        output_array[counter][item] = 1

    return output_array

lag =3
"""
X = pd.read_pickle("Data/X" + str(lag-1) + "lagUAHCARLAData")
y = pd.read_pickle("Data/y"+ str(lag-1) + "lagUAHCARLAData")


X = pd.read_pickle("Data/X_CARLA_" + str(lag-1))
y = pd.read_pickle("Data/y_CARLA_" + str(lag-1))
"""
if path.exists("Data/X" + str(lag - 1) + "Acc10HzData") == True:
    X = pd.read_pickle("Data/X" + str(lag - 1) + "Acc10HzData")
    y = pd.read_pickle("Data/y" + str(lag - 1) + "Acc10HzData")
else:
    # Obtaining the csv file having all outputs on 1 coloumn
    X, y,lengths = returnXYData(lag - 1, 5,True)

    X.to_pickle("Data/X" + str(lag - 1) + "Acc10HzData")
    y.to_pickle("Data/y" + str(lag - 1) + "Acc10HzData")


X = X.to_numpy()
y = y.to_numpy()
y = revert_onehot_encoding(y)

#X_train, X_test, y_train, y_test = train_test_split( X, y, test_size=0.2)

skf_5fold_shuffled = StratifiedKFold(n_splits=5,shuffle=True)

#'kernel':['linear','poly','rbf','sigmoid', 'precomputed']

parameters = {'C':[20],'kernel':['rbf']}
svm = svm.SVC()

clf = GridSearchCV(svm,parameters,n_jobs=-1,cv=skf_5fold_shuffled,scoring=['accuracy','precision_weighted','recall_weighted','f1_weighted'],refit='f1_weighted',return_train_score=True,verbose = 3)
clf.fit(X,y)

#y_pred = clf.predict(X_test)

#print("Acc:" + str(accuracy_score(y_test,y_pred)))
#print("F1 Score Weighted:" + str(f1_score(y_test,y_pred,average='weighted')))

print("LAG = " + str(lag))
print(clf.cv_results_.keys())
for key in clf.cv_results_.keys():
    print(str(key) + ":" + str(clf.cv_results_[key]))


#from sklearn.metrics import classification_report
#print(classification_report(y_test, y_pred))