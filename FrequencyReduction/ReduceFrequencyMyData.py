import pandas as pd
import csv
import math

def convert_seconds_to_nanoseconds(seconds):
    return seconds * 1000000000


def reduce_frequency(dataframe, new_frequency):
    total_datapoints = dataframe.shape[0]
    new_frequency_dataframe = pd.DataFrame()

    datapoints_per_group = total_datapoints/new_frequency

    if datapoints_per_group > 5.4:
        print("ere")
        #raise Exception("Inaccurate Divisions")

    datapoints_per_group = math.floor(datapoints_per_group)

    starting_index = 0
    stop_index = starting_index + datapoints_per_group

    for i in range(new_frequency):
        temp = dataframe[starting_index:stop_index].mean()
        temp = pd.Series(dataframe.iloc[0][0],index=['timestamp'],name="dad").append(temp)
        temp.reindex(dataframe.columns)


        new_frequency_dataframe= new_frequency_dataframe.append(temp,ignore_index=True)
        starting_index = starting_index + datapoints_per_group
        stop_index = stop_index + datapoints_per_group

        if i == new_frequency -2:
            stop_index = total_datapoints

    return new_frequency_dataframe








eventDataframes = []
# eventDataframes.append(pd.DataFrame())

driverNo = [7]
startTimes = [44604564044130]
new_second_start = [44604665450901,45258680545287,46874952356753,47159967614926,47803960728430,48734984988752,49300984535724,50148005216494]
main_dir = "C:\\Users\\attar\\Desktop\\FYP\\Data"

for driverNoIndex in range(1):
    print("Starting player: " + str(driverNo[driverNoIndex]))
    linearAccDataDir = main_dir + "\\Data Collection " + str(driverNo[driverNoIndex]) + "\\LINEAR ACCELEROMETER2021-03-29.csv"


    linearAccData = pd.read_csv(linearAccDataDir)


    # Read each row of the input csv file as list
    startTime = startTimes[driverNoIndex]

    previous_time = new_second_start[driverNoIndex]
    counter = 0


    output_dataframe = pd.DataFrame()
    temp_dataframe = pd.DataFrame()
    for index, dataRow in linearAccData.iterrows():
        if dataRow[2] < new_second_start[counter]:
            continue

        if index == 0:
            output_dataframe = output_dataframe.append(dataRow)

        if dataRow[2] >= previous_time + convert_seconds_to_nanoseconds(1) and dataRow[2] < previous_time + convert_seconds_to_nanoseconds(1.1):
            output_dataframe = output_dataframe.append(reduce_frequency(temp_dataframe, 10))

            previous_time = dataRow[2]
            temp_dataframe.drop(temp_dataframe.index, inplace=True)
            temp_dataframe = temp_dataframe.append(dataRow)

        elif dataRow[2] >= previous_time + convert_seconds_to_nanoseconds(1.1):
            print(dataRow[2])
            print(dataRow[1])
            counter += 1
            if counter == 8:
                break
            previous_time = new_second_start[counter]

            temp_dataframe.drop(temp_dataframe.index, inplace=True)


        else:
            temp_dataframe = temp_dataframe.append(dataRow)

    output_dataframe.to_csv(main_dir + "\\LINEAR ACCELEROMETER2021-03-2910Hz.csv",index=False)





